# ft_ls

Ce **projet scolaire** était un projet système de l'école [42 Lyon](https://www.42lyon.fr/).

## Objectif

Pour tout connaitre du filesystem, de la façon dont sont rangés les fichiers et répertoires, codez par vous-même une des commandes les plus utilisées : ls .

## Résultat
Un programme avec des fonctionnalités de la commande "ls" dont la lecture récursive des répertoires.

> PS: Le répertoire “decouverte” était le répertoire servant “d’atelier” contenant mes différentes expérimentations sur les fonctionnalités nécessaires.