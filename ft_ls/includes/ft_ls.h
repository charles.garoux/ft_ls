/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_ls.h                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/06/06 12:11:02 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/24 12:49:50 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# define DEBUG_MODE 0

# if DEBUG_MODE == 1
#  define DEBUG_PRINT(header, reason)\
	debug_print_error(header, __FILE__, __func__, __LINE__ -1, reason)
# else
#  define DEBUG_PRINT(header, reason)
# endif

# include "garbage_libft.h"
# include <stdio.h>
# include <unistd.h>
# include <sys/stat.h>
# include <pwd.h>
# include <grp.h>
# include <time.h>
# include <limits.h>
# include <dirent.h>
# include <errno.h>

/*
**	Les define qui suivent sont pour la compilation Linux et MacOS.
**	Le programme sera fonctionnel sur linux et MacOS un fois compile.
*/

# if defined(linux) || defined(__linux__)
#  define __OS__ "Linux"
#  define BLOCK 2
#  include <sys/sysmacros.h>
#  define MODIF_TIME st_mtim
# elif defined(__APPLE__) || defined(__MACH__)
#  define __OS__ "MacOS"
#  define BLOCK 1
#  define MODIF_TIME st_mtimespec
#  include <sys/xattr.h>
# endif

/*
** #pragma pack(1) permet de supprimer le padding dans les strucuture
** et donc de limiter les place prise par les structures memoire et permet
** aussi en passant une structe en tableau d'octet de ne pas avoir d'octet
** avec un contenue qui n'a pas lieu d'etre. Il permet d'utiliser la fonction
** ft_structcmp() qui compare deux structure.
*/

# pragma pack(1)

typedef int					t_options;
typedef struct s_list_elem	t_list_elem;
typedef struct s_directory	t_directory;
typedef struct s_layout		t_layout;
typedef int					(*t_elem_cmp)(t_list_elem*, t_list_elem*);

# define FLAG_LOW_L 1
# define FLAG_UP_R 2
# define FLAG_LOW_A 4
# define FLAG_LOW_R 8
# define FLAG_LOW_T 16

struct		s_layout
{
	size_t	hardlink_max;
	size_t	user_name_max;
	size_t	group_name_max;
	size_t	size_max;
	size_t	major_max;
	size_t	minor_max;
};

struct		s_directory
{
	long long int	block;
	t_list_elem		*element;
	t_list_elem		*begin_content;
	t_layout		*layout;
};

struct		s_list_elem
{
	char			*name;
	char			*path;
	struct stat		*data;
	t_directory		*dir_data;
	t_directory		*parent;
	char			*user_name;
	char			*group_name;
	char			type;
	char			right[10];
	char			*modif_date;
	char			*size;
	char			*hardlink;
	char			*link_target;
	t_list_elem		*next;
	t_list_elem		*prev;
	char			*major;
	char			*minor;
	time_t			modif_date_timestamp;
	t_list_elem *link_element;
	t_list_elem *link_parent;
};

/*
**	sources/main.c
** print_options est une fonction de debug
*/

void		debug_print_error(char *type, char *file, const char *function,
	int line_number, char *error_reason);

/*
** sources/parse_options.c
** print_options est une fonction de debug
*/

t_options	parse_options(int argc, char **argv);
void		print_options(t_options options);

/*
** sources/parse_paths.c
** print_paths_list est une fonction de debug
*/

char		**parse_paths(int argc, char **argv);
void		print_paths_list(char **paths_list);

/*
** sources/listing.c
*/

int			listing(char **paths_list, t_options options);

/*
**	sources/print.c
*/

void		print_element(t_list_elem *element, t_layout *layout,
	t_options options);
void	print_directory(t_directory *directory, t_options options);
int			listing_directory(t_list_elem *element, t_options options);

/*
** sources/element_creation.c
*/

int			free_element(t_list_elem *element);
int			free_element_list(t_list_elem *element);
t_list_elem *create_element(char *path, t_directory *parent_dir,
	t_list_elem *prev_element, t_layout *layout);
t_list_elem	*create_target_link_element(char *link_path, char *link_path_target);

/*
** sources/element_filling.c
*/

int			fill_element(t_list_elem *element, struct stat *data, char *path,
	t_directory *parent_dir);
char		*get_file_name(t_list_elem *element, char *path);

/*
** sources/element_filling_2.c
*/

char		*get_hardlink_nbr(nlink_t st_nlink);
void		set_special_right(t_list_elem *element, mode_t right);
void		set_right(t_list_elem *element, mode_t right);
char		*get_group_name(gid_t gid);

/*
** sources/element_filling_special.c
*/

char		*get_link_target(char *path_to_link);
int			set_major_minor(t_list_elem *element, dev_t st_rdev);
int			fill_special_element(t_list_elem *element, struct stat *data);

/*
** sources/modif_date.c
*/

char		*get_modif_date(time_t time);
char		*del_day_week(char *date);
char		*format_date(time_t modif_time, char *date);
char		*format_far_date(char *date);

/*
** sources/directory_creation.c
*/

t_directory *create_directory(t_list_elem *element_dir, t_options options);
t_list_elem *search_dir(t_directory *directory);
int			free_directory_element(t_list_elem *element_dir);

/*
**	sources/layout.c
*/

void		update_layout(t_layout *layout, t_list_elem *element);
t_layout	*create_layout(void);

/*
** sources/triple_conctat.c
*/

char		*triple_conctat(char *str1, char *str2, char *str3);

/*
** sources/sort_list.c
*/

t_list_elem *sort_list(t_list_elem *begin_element, t_options options);
int			cmp_name_ascii(t_list_elem *a, t_list_elem *b);
int			cmp_modif_date(t_list_elem *a, t_list_elem *b);

/*
** sources/quicksort_element_list.c
*/

void		quick_sort(t_list_elem *start, t_list_elem *end, t_elem_cmp ft_cmp);
void		rev_quick_sort(t_list_elem *start, t_list_elem *end,
	t_elem_cmp ft_cmp);

/*
**	sources/print_error.c
*/

void		*print_error(char *file_name);

/*
**	sources/padding_data.c
*/

char		*new_padding(char *last_padding, size_t new_size);
char		*padd_hardlink(char *data_str, size_t size);
char		*padd_user_name(char *data_str, size_t size);
char		*padd_group_name(char *data_str, size_t size);
char		*padd_size(char *data_str, size_t size);

/*
**	sources/padding_major_minor.c
*/

char		*padd_major(char *data_str, size_t size);
char		*padd_minor(char *data_str, size_t size);

/*
**	sources/print_extended_element.c
*/

void		print_extended_element(t_list_elem *element, t_layout *layout);

/*
**	sources/listing_multi_path.c
*/

int			listing_multi_path(char **paths_list, t_options options);

#endif
