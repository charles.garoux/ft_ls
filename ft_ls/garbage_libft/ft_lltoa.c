/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_lltoa.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/18 17:36:00 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/18 17:36:40 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "garbage_libft.h"

static size_t	ft_nbrlen(long long nbr)
{
	size_t		len;
	long long	divider;

	len = 0;
	if (nbr < 0)
		len++;
	else if (nbr == 0)
		return (1);
	if (nbr / 1000000000000000000 != 0)
		return (len + 19);
	divider = 1;
	while (nbr / divider)
	{
		divider *= 10;
		len++;
	}
	return (len);
}

char			*ft_lltoa(long long int nbr)
{
	int		len;
	char	*nbr_str;
	int		neg;

	len = ft_nbrlen(nbr);
	neg = (nbr < 0) ? 1 : 0;
	if (!(nbr_str = (char *)garbage_malloc(sizeof(char) * (len + 1))))
		return (NULL);
	nbr_str[len--] = '\0';
	while (len >= 0)
	{
		nbr_str[len] = ft_abs(nbr % 10) + '0';
		nbr = nbr / 10;
		len--;
	}
	if (neg)
		nbr_str[0] = '-';
	return (nbr_str);
}
