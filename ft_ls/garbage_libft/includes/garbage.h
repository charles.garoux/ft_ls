/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   garbage.h                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/19 18:01:11 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/19 18:01:15 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef GARBAGE_H
# define GARBAGE_H

# include <stdlib.h>

/*
** Structure of the chained list to store each allocated element.
*/

typedef struct s_garblst	t_garblst;

struct		s_garblst
{
	void		*allocated;
	t_garblst	*next;
};

extern t_garblst	*g_garbage;

void		garbage_free(void *ptr);
void		*garbage_malloc(size_t size);
void		free_g_garbage_lst(void) __attribute__((destructor));

#endif
