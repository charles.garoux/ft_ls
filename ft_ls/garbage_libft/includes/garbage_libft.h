/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   garbage_libft.h                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/19 16:53:30 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/19 17:57:17 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef GARBAGE_LIBFT_H
# define GARBAGE_LIBFT_H

# include <unistd.h>
# include <stdlib.h>
# include <string.h>
# include "styles.h"
# include "garbage.h"

void			ft_bzero(void *s, size_t n);
char			*ft_itoa(int nbr);
char			*ft_lltoa(long long int nbr);
void			*ft_memalloc(size_t size);
void			*ft_memcpy(void *dst, const void *src, size_t n);
void			*ft_memmove(void *dst, const void *src, size_t len);
size_t			ft_pstrlen(const char *str);
void			ft_putchar(char c);
void			ft_putendl(char const *s);
void			ft_putstr(const char *s);
int				ft_strcmp(const char *s1, const char *s2);
char			*ft_strdup(const char *src);
size_t			ft_strlen(const char *s);
char			*ft_strndup(const char *src, size_t n);
char			*ft_strnew(size_t size);
size_t			ft_strnlen(const char *s, size_t maxlen);
void			ft_putstr_fd(char const *s, int fd);
unsigned int	ft_abs(int nbr);
void			*ft_memset(void *b, int c, size_t len);

#endif
