/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/07/23 02:37:56 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/19 13:24:43 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void	debug_print_error(char *type, char *file, const char *function,\
		int line_number, char *error_reason)
{
	dprintf(2, RED"[%s]"WHITE" %s:%d > %s() > %s"INIT"\n", type, file,\
			line_number, function, error_reason);
	perror(""YELLOW"\tperror");
	ft_putstr_fd(INIT, 2);
}

/*
**	Une fois les DEBUG_PRINT et les DEBUG_MODE supprimer, main() est a la norme
*/

int		main(int argc, char **argv)
{
	t_options	options;
	char		**paths_list;
	int			ret;

	if (DEBUG_MODE == 1)
	{
		printf(H_YELLOW"[Information compilation] OS=%s DEBUG_MODE=%s"INIT"\n",\
				__OS__, (DEBUG_MODE) ? "actif" : "inactif");
		ft_putstr("=================================================START=====\
				===============================================\n");
	}
	options = 0;
	paths_list = NULL;
	if (argc > 1)
	{
		if ((options = parse_options(argc, argv)) == -1)
		{
			DEBUG_PRINT("Erreur", "parse_options() == -1");
			return (1);
		}
		if (DEBUG_MODE == 1)
			print_options(options);
	}
	if (NULL == (paths_list = parse_paths(argc, argv)))
	{
		DEBUG_PRINT("Erreur", "parse_paths() == -1");
		return (2);
	}
	if (DEBUG_MODE == 1)
		print_paths_list(paths_list);
	if (-1 == (ret = listing(paths_list, options)))
	{
		DEBUG_PRINT("Erreur", "listing() == -1");
		return (3);
	}
	garbage_free(paths_list);
	if (DEBUG_MODE == 1)
		ft_putstr("=================================================END==\
		==================================================\n");
	if (ret != 0)
		return (4);
	return (0);
}
