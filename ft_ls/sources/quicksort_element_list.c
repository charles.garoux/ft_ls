/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   quicksort_element_list.c                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/17 15:11:42 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/17 15:12:28 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

int			swap_elements(t_list_elem **a, t_list_elem **b)
{
	t_list_elem *temp;
	t_list_elem *a_next;
	t_list_elem *a_prev;
	t_list_elem *b_next;
	t_list_elem *b_prev;

	if (NULL == (temp = (t_list_elem*)garbage_malloc(sizeof(*temp))))
	{
		DEBUG_PRINT("Erreur", "malloc() == NULL");
		return (-1);
	}
	a_next = (*a)->next;
	a_prev = (*a)->prev;
	b_next = (*b)->next;
	b_prev = (*b)->prev;
	memcpy(temp, *a, sizeof(t_list_elem));
	memcpy(*a, *b, sizeof(t_list_elem));
	memcpy(*b, temp, sizeof(t_list_elem));
	(*a)->next = a_next;
	(*a)->prev = a_prev;
	(*b)->next = b_next;
	(*b)->prev = b_prev;
	garbage_free(temp);
	return (0);
}

/*
**	La fonction de partitionnement :
**	On Considere "pivot" le dernier element de la liste entree.
**	la fonction va placer les elements inferieurs ou egaux au pivot avant lui
**	dans la liste et les elements superieur apres.
*/

t_list_elem	*partition(t_list_elem *start, t_list_elem *pivot,
	t_elem_cmp ft_cmp)
{
	t_list_elem *index;
	t_list_elem *prev_start;

	index = start;
	prev_start = start->prev;
	while (index != pivot)
	{
		if (ft_cmp(index, pivot) <= 0)
		{
			prev_start = (prev_start == NULL) ? start : prev_start->next;
			if (-1 == (swap_elements(&prev_start, &index)))
			{
				DEBUG_PRINT("Erreur", "swap_elements() == NULL");
				return (NULL);
			}
		}
		index = index->next;
	}
	prev_start = (prev_start == NULL) ? start : prev_start->next;
	if (-1 == (swap_elements(&prev_start, &index)))
	{
		DEBUG_PRINT("Erreur", "swap_elements() == NULL");
		return (NULL);
	}
	return (prev_start);
}

t_list_elem	*rev_partition(t_list_elem *start, t_list_elem *pivot,
	t_elem_cmp ft_cmp)
{
	t_list_elem *index;
	t_list_elem *prev_start;

	index = start;
	prev_start = start->prev;
	while (index != pivot)
	{
		if (ft_cmp(index, pivot) >= 0)
		{
			prev_start = (prev_start == NULL) ? start : prev_start->next;
			if (-1 == (swap_elements(&prev_start, &index)))
			{
				DEBUG_PRINT("Erreur", "swap_elements() == NULL");
				return (NULL);
			}
		}
		index = index->next;
	}
	prev_start = (prev_start == NULL) ? start : prev_start->next;
	if (-1 == (swap_elements(&prev_start, &index)))
	{
		DEBUG_PRINT("Erreur", "swap_elements() == NULL");
		return (NULL);
	}
	return (prev_start);
}

/*
**	Fonction recursion de quicksort.
**	Lance le partitionnement et avec le pivot fixe lance les 2 autres
**	partitionnement.
*/

void		quick_sort(t_list_elem *start, t_list_elem *end,
	t_elem_cmp ft_cmp)
{
	t_list_elem *fixed_pivot;

	if (end != NULL && start != end && start != end->next)
	{
		fixed_pivot = partition(start, end, ft_cmp);
		quick_sort(start, fixed_pivot->prev, ft_cmp);
		quick_sort(fixed_pivot->next, end, ft_cmp);
	}
}

void		rev_quick_sort(t_list_elem *start, t_list_elem *end,
	t_elem_cmp ft_cmp)
{
	t_list_elem *fixed_pivot;

	if (end != NULL && start != end && start != end->next)
	{
		fixed_pivot = rev_partition(start, end, ft_cmp);
		rev_quick_sort(start, fixed_pivot->prev, ft_cmp);
		rev_quick_sort(fixed_pivot->next, end, ft_cmp);
	}
}
