/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   triple_concate.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/18 17:17:26 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/18 17:17:56 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

char	*ft_pstrcpy(char *dst, const char *src)
{
	size_t i;

	if (dst == NULL || src == NULL)
		return (NULL);
	i = 0;
	while (src[i])
	{
		dst[i] = src[i];
		i++;
	}
	dst[i] = '\0';
	return (dst);
}

char	*triple_conctat(char *str1, char *str2, char *str3)
{
	size_t	len1;
	size_t	len2;
	size_t	len3;
	char	*new_str;

	len1 = ft_pstrlen(str1);
	len2 = ft_pstrlen(str2);
	len3 = ft_pstrlen(str3);
	if (NULL == (new_str = (char*)garbage_malloc(len1 + len2 + len3 + 1)))
		return (NULL);
	ft_pstrcpy(new_str, str1);
	ft_pstrcpy(new_str + len1, str2);
	ft_pstrcpy(new_str + len1 + len2, str3);
	return (new_str);
}
