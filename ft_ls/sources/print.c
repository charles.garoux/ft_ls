/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/19 13:48:56 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/19 13:48:57 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_element(t_list_elem *element, t_layout *layout, t_options options)
{
	if (options & FLAG_LOW_L)
		print_extended_element(element, layout);
	else
	{
		if (element->link_element)
			print_directory(element->link_element->dir_data, options);
		else
			ft_putendl(element->name);
	}
}

void	print_list(t_list_elem *element, t_layout *layout, t_options options)
{
	while (element != NULL)
	{
		print_element(element, layout, options);
		element = element->next;
	}
}

void	print_list_and_dir(t_list_elem *element, t_layout *layout,\
	t_options options)
{
	while (element != NULL)
	{
		if (element->type == 'd')
			listing_directory(element, options);
		else
			print_element(element, layout, options);
		element = element->next;
	}
}

void	print_directory(t_directory *directory, t_options options)
{
	char *block_str;

	if (directory->element->parent != NULL ||
		(directory->element->parent == NULL &&
		directory->element->link_element &&
		(directory->element->next || directory->element->prev)))
	{
		write(1, "\n", 1);
		ft_putstr(directory->element->path);
		write(1, ":\n", 2);
	}
	if (options & FLAG_LOW_L)
	{
		block_str = ft_lltoa(directory->block / BLOCK);
		write(1, "total ", 6);
		ft_putstr(block_str);
		write(1, "\n", 1);
		garbage_free(block_str);
	}
	print_list(directory->begin_content, directory->layout, options);
}

int		listing_directory(t_list_elem *element, t_options options)
{
	t_directory *directory;
	t_list_elem *sub_element;

	if (NULL == (directory = create_directory(element, options)))
	{
		DEBUG_PRINT("Erreur", "create_directory() == NULL");
		return (-1);
	}
	print_directory(directory, options);
	if (options & FLAG_UP_R)
	{
		while (NULL != (sub_element = search_dir(directory)))
		{
			listing_directory(sub_element, options);
			free_element(sub_element);
		}
	}
	return (1);
}
