/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   listing_multi_path.c                             .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/19 13:06:24 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/19 15:30:23 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void		print_multi_path(t_list_elem *element, t_layout *layout,\
		t_options options)
{
	t_list_elem	*begin_element;

	begin_element = element;
	while (element != NULL)
	{
		if (element->type != 'd' &&
			(element->link_element == NULL || (options & FLAG_LOW_L)))
			print_element(element, layout, options);
		element = element->next;
	}
	element = begin_element;
	while (element != NULL)
	{
		if (element->type == 'd')
			listing_directory(element, options);
		else if ((options & FLAG_LOW_L) == 0 && element->link_element)
		{
			listing_directory(element, options);
		}
		element = element->next;
	}
}

t_list_elem	*create_next_element(char **path, t_list_elem *prev_element,
		t_layout *layout)
{
	t_list_elem	*element;

	if (NULL == (element = create_element(*path, NULL, prev_element, layout))
			|| (t_list_elem*)1 == element)
	{
		if (NULL == element)
			return (NULL);
		else if (path[1])
			return (create_next_element(path + 1, prev_element, layout));
	}
	if (element->type == 'd')
		element->parent = (t_directory*)1;
	return (element);
}

t_list_elem	*create_list_element(char **paths_list, t_layout *layout)
{
	t_list_elem	*element;
	t_list_elem	*prev_element;
	t_list_elem	*init_element;

	prev_element = NULL;
	init_element = NULL;
	while (*paths_list)
	{
		if (NULL == (element = create_element(*paths_list, NULL, prev_element,\
			layout)) || (t_list_elem*)1 == element)
		{
			if (NULL == element)
				return (NULL);
		}
		else
		{
			if (element->type == 'd')
				element->parent = (t_directory*)1;
			if (init_element == NULL)
				init_element = element;
			prev_element = element;
		}
		paths_list++;
	}
	return (init_element);
}

int			listing_multi_path(char **paths_list, t_options options)
{
	t_list_elem		*init_element;
	t_layout		*layout;

	layout = create_layout();
	if (NULL == (init_element = create_list_element(paths_list, layout)))
		return (-1);
	print_multi_path(init_element = sort_list(init_element, options),
		layout, options);
	garbage_free(layout);
	free_element_list(init_element);
	return (0);
}
