/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   listing.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/19 13:51:42 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/19 13:52:33 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** NOTE:  Quand plusieurs chemins sont passe alors pour ces elements
**        l'attribut "parent" est mis a 1 pour afficher l'entete des repertoire
**        (voir fonction : print_directory).
*/

/*
** Une fois les DEBUG_PRINT supprimer, listing() est a la norme
*/

int		listing(char **paths_list, t_options options)
{
	t_list_elem	*element;

	if (paths_list[1] == NULL)
	{
		if (NULL == (element = create_element(paths_list[0], NULL, NULL, NULL)))
		{
			DEBUG_PRINT("Erreur", "create_element() == NULL");
			return (-1);
		}
		if ((t_list_elem*)1 == element)
			return (1);
		if (element->type == 'd' ||
			((options & FLAG_LOW_L) == 0 && element->link_element))
		{
			if (-1 == (listing_directory(element, options)))
			{
				DEBUG_PRINT("Erreur", "listing_directory() == NULL");
				return (-1);
			}
		}
		else
			print_element(element, NULL, options);
		free_element(element);
	}
	else
	{
		if (-1 == listing_multi_path(paths_list, options))
			return (-1);
	}
	return (0);
}
