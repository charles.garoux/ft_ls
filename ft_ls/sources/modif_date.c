/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   modif_date.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/18 12:21:20 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/18 12:24:46 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

char	*format_far_date(char *date)
{
	register unsigned int	index;
	unsigned int			marker;

	index = 0;
	while (date[index])
		index++;
	while (date[index] != ' ')
		index--;
	marker = index;
	index = index - 2;
	while (date[index] != ' ')
		index--;
	index++;
	date[index] = ' ';
	ft_memmove(date + index, date + marker, 5);
	ft_bzero(date + index + 5, 10);
	return (date);
}

char	*format_date(time_t modif_time, char *date)
{
	time_t					now;
	register unsigned int	index;

	time(&now);
	index = 0;
	if (modif_time < now - 60 * 60 * 24 * 30 * 6)
		return (format_far_date(date));
	else
	{
		while (date[index])
			index++;
		while (date[index] != ':')
			index--;
		ft_bzero(date + index, 5);
		return (date);
	}
	return (date);
}

/*
** del_day_week est optimisable
*/

char	*del_day_week(char *date)
{
	register unsigned int index;

	index = 0;
	while (date[index] != ' ')
		index++;
	index++;
	ft_memmove(date, date + index, ft_strlen(date + index));
	while (date[index] != '\n')
		index++;
	ft_bzero(date + index, ft_strlen(date + index));
	return (date);
}

char	*get_modif_date(time_t time)
{
	char	*modif_date;

	if (NULL == (modif_date = ctime(&time)))
	{
		DEBUG_PRINT("Erreur", "ctime() == NULL");
		return (NULL);
	}
	if (NULL == (modif_date = ft_strdup(modif_date)))
	{
		DEBUG_PRINT("Erreur", "ft_strdup() == NULL");
		return (NULL);
	}
	modif_date = del_day_week(modif_date);
	modif_date = format_date(time, modif_date);
	return (modif_date);
}
