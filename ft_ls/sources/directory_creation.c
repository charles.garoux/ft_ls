/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   directory_creation.c                             .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/19 15:42:37 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/19 16:01:02 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

t_list_elem	*search_dir(t_directory *directory)
{
	t_list_elem	*element;

	element = directory->begin_content;
	while ((element != NULL && (element->type != 'd' ||
	(element->name[0] == '.' && (element->name[1] == '\0' ||
	(element->name[1] == '.' && element->name[2] == '\0'))))))
		element = element->next;
	if (element == NULL)
		return (NULL);
	return (element);
}

int			free_directory_element(t_list_elem *element_dir)
{
	free_element_list(element_dir->dir_data->begin_content);
	garbage_free(element_dir->dir_data->layout);
	garbage_free(element_dir->dir_data);
	return (1);
}

/*
** Une fois les DEBUG_PRINT supprimer, open_directory_from_elem() est a la norme
*/

t_directory	*open_directory_from_elem(t_directory *directory, t_options options)
{
	DIR				*data_dir;
	struct dirent	*pdirent;
	char			*element_path;
	t_list_elem		*new_element;

	new_element = NULL;
	if (NULL == (data_dir = opendir(directory->element->path)))
		return ((t_directory*)print_error(directory->element->path));
	while ((pdirent = readdir(data_dir)) != NULL)
	{
		if (pdirent->d_name[0] != '.' || options & FLAG_LOW_A)
		{
			if (NULL == (element_path = triple_conctat(directory->element->path,
				"/", pdirent->d_name)))
			{
				DEBUG_PRINT("Erreur", "triple_conctat() == NULL");
				return (NULL);
			}
			if (NULL == (new_element = create_element(element_path, directory,
				new_element, directory->layout)))
			{
				DEBUG_PRINT("Erreur", "create_element() == NULL");
				return (NULL);
			}
			garbage_free(element_path);
		}
	}
	closedir(data_dir);
	return (directory);
}

t_directory	*create_directory(t_list_elem *element_dir, t_options options)
{
	t_directory *directory;

	if (NULL == (directory = (t_directory*)garbage_malloc(sizeof(*directory))))
	{
		DEBUG_PRINT("Erreur", "malloc() == NULL");
		return (NULL);
	}
	directory->element = element_dir;
	element_dir->dir_data = directory;
	directory->block = 0;
	if (NULL == (directory->layout = create_layout()))
	{
		DEBUG_PRINT("Erreur", "create_layout() == NULL");
		return (NULL);
	}
	directory->begin_content = NULL;
	if (NULL == (open_directory_from_elem(directory, options)))
	{
		DEBUG_PRINT("Erreur", "open_directory_from_elem() == NULL");
		return (NULL);
	}
	directory->begin_content = sort_list(directory->begin_content, options);
	return (directory);
}
