/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   parse_paths.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/18 17:12:43 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/19 13:42:09 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void		print_paths_list(char **paths_list)
{
	unsigned int index;

	index = 0;
	ft_putstr("\nDebug : paths_list :\n");
	if (paths_list == NULL)
		ft_putstr("\tpaths_list == NULL");
	while (paths_list[index] != NULL)
	{
		ft_putendl(paths_list[index]);
		index++;
	}
	ft_putendl(paths_list[index]);
	ft_putstr("================================\
			=================================================\n");
}

static int	count_paths(int argc, char **argv)
{
	register int	index;
	int				nbr_paths;

	index = 1;
	nbr_paths = 0;
	while (index < argc)
	{
		if (argv[index][0] ^ '-')
			nbr_paths++;
		index++;
	}
	return (nbr_paths);
}

/*
**  Ne par copier l'argument economise la memoire et des appels systemes
*/

static char	*check_path(char *argument)
{
	if (*argument == '-')
		return (NULL);
	return (argument);
}

char		**parse_paths(int argc, char **argv)
{
	register int	index_arg;
	register int	index_path;
	char			**paths_list;
	int				nbr_paths;

	index_arg = 1;
	index_path = 0;
	nbr_paths = count_paths(argc, argv);
	if (NULL == (paths_list = (char**)garbage_malloc(sizeof(*paths_list) *\
					(((nbr_paths != 0) ? nbr_paths : 1) + 1))))
		return (NULL);
	if (nbr_paths == 0)
		paths_list[0] = ".";
	else
		while (index_arg < argc && index_path < nbr_paths)
		{
			if (NULL != (paths_list[index_path] = check_path(argv[index_arg])))
				index_path++;
			index_arg++;
		}
	paths_list[(nbr_paths != 0) ? nbr_paths : 1] = NULL;
	return (paths_list);
}
