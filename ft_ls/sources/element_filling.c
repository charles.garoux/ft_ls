/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   element_filling.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/18 17:37:52 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/18 18:34:32 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

char	*get_user_name(uid_t uid)
{
	struct passwd	*user_data;
	char			*user_name;

	if (NULL == (user_data = getpwuid(uid)))
	{
		if (NULL == (user_name = ft_itoa(uid)))
		{
			DEBUG_PRINT("Erreur", "ft_itoa() == NULL");
			return (NULL);
		}
	}
	else if (NULL == (user_name = ft_strdup(user_data->pw_name)))
	{
		DEBUG_PRINT("Erreur", "ft_strdup() == NULL");
		return (NULL);
	}
	return (user_name);
}

char	get_file_type(mode_t type)
{
	if (type == S_IFBLK)
		return ('b');
	if (type == S_IFCHR)
		return ('c');
	if (type == S_IFDIR)
		return ('d');
	if (type == S_IFIFO)
		return ('p');
	if (type == S_IFLNK)
		return ('l');
	if (type == S_IFREG)
		return ('-');
	if (type == S_IFSOCK)
		return ('s');
	return ('?');
}

/*
** Une fois les DEBUG_PRINT supprimer, get_path() est a la norme
*/

char	*get_path(char *path)
{
	char			*new_path;
	size_t			size;
	register int	index;

	size = 0;
	index = 0;
	while (path[index])
	{
		if (!(path[index++] == '/' && path[index] == '/'))
			size++;
	}
	index = 0;
	if (NULL == (new_path = (char*)garbage_malloc(size + 1)))
	{
		DEBUG_PRINT("Erreur", "malloc() == NULL");
		return (NULL);
	}
	size = 0;
	while (path[index])
	{
		if (!(path[index] == '/' && path[index + 1] == '/'))
			new_path[size++] = path[index];
		index++;
	}
	new_path[size] = '\0';
	return (new_path);
}

/*
**	Il est possible d'optimiser la fonction get_file_name
*/

char	*get_file_name(t_list_elem *element, char *path)
{
	char			*name;
	register size_t	index;

	index = 0;
	if (element->parent == NULL || element->parent->element == NULL)
		name = ft_strdup(path);
	else
	{
		index = ft_strlen(path);
		while (index > 0 && path[index - 1] != '/')
			index--;
		name = ft_strdup(path + index);
	}
	return (name);
}

/*
** Une fois les DEBUG_PRINT supprimer, fill_element() est a la norme
*/

int		fill_element(t_list_elem *element, struct stat *data,
	char *path, t_directory *parent_dir)
{
	if (NULL == (element->path = get_path(path)))
	{
		DEBUG_PRINT("Erreur", "get_path() == NULL");
		return (-1);
	}
	element->name = get_file_name(element, path);
	element->type = get_file_type(data->st_mode & S_IFMT);
	if (NULL == (element->user_name = get_user_name(data->st_uid)))
	{
		DEBUG_PRINT("Erreur", "get_user_name() == NULL");
		return (-1);
	}
	if (NULL == (element->group_name = get_group_name(data->st_gid)))
	{
		DEBUG_PRINT("Erreur", "get_group_name() == NULL");
		return (-1);
	}
	set_right(element, data->st_mode);
	if (NULL == (element->modif_date = get_modif_date(data->MODIF_TIME.tv_sec)))
	{
		DEBUG_PRINT("Erreur", "get_group_name() == NULL");
		return (-1);
	}
	element->modif_date_timestamp = data->MODIF_TIME.tv_sec;
	if (-1 == (fill_special_element(element, data)))
	{
		DEBUG_PRINT("Erreur", "fill_special_elements() == -1");
		return (-1);
	}
	if (NULL == (element->hardlink = get_hardlink_nbr(data->st_nlink)))
	{
		DEBUG_PRINT("Erreur", "get_hardlink_nbr() == NULL");
		return (-1);
	}
	if (parent_dir != NULL)
		update_layout(parent_dir->layout, element);
	element->dir_data = NULL;
	element->next = NULL;
	element->prev = NULL;
	return (1);
}
