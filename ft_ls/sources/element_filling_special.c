/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   element_filling_special.c                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/18 17:42:28 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/18 18:38:00 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

char	*get_link_target(char *path_to_link)
{
	char		*target;
	char		buffer[PATH_MAX];
	long int	target_len;

	if (-1 == (target_len = readlink(path_to_link, buffer, PATH_MAX)))
	{
		DEBUG_PRINT("Erreur", "readlink() == NULL");
		return ((char*)1);
	}
	if (NULL == (target = ft_strndup(buffer, target_len)))
	{
		DEBUG_PRINT("Erreur", "readlink() == NULL");
		return (NULL);
	}
	return (target);
}

int		set_major_minor(t_list_elem *element, dev_t st_rdev)
{
	if (NULL == (element->major = ft_lltoa(major(st_rdev))))
	{
		DEBUG_PRINT("Erreur", "ft_lltoa() == NULL");
		return (-1);
	}
	if (NULL == (element->minor = ft_lltoa(minor(st_rdev))))
	{
		DEBUG_PRINT("Erreur", "ft_lltoa() == NULL");
		return (-1);
	}
	return (0);
}

int		fill_special_element(t_list_elem *element, struct stat *data)
{
	if (NULL == (element->size = ft_lltoa(data->st_size)))
	{
		DEBUG_PRINT("Erreur", "ft_lltoa() == NULL");
		return (-1);
	}
	if (-1 == (set_major_minor(element, data->st_rdev)))
	{
		DEBUG_PRINT("Erreur", "set_major_minor() == NULL");
		return (-1);
	}
	if (element->type == 'l')
	{
		if (NULL == (element->link_target = get_link_target(element->path)))
		{
			DEBUG_PRINT("Erreur", "get_link_target() == NULL");
			return (-1);
		}
		else if ((char*)1 == element->link_target)
			element->link_target = NULL;
		if (!element->parent && NULL == (element->link_element =
			create_target_link_element(element->path, element->link_target)))
		{
			DEBUG_PRINT("Erreur", "create_target_link_element() == NULL");
			return (-1);
		}
		element->link_element = NULL;
	}
	else
	{
		element->link_element = NULL;
		element->link_target = NULL;
	}
	return (0);
}
