/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   parse_options.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/07/23 02:46:51 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/17 18:21:33 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** Debug, affiche toutes les options active
*/

void		print_options(t_options options)
{
	ft_putstr("Debug : Options :");
	if (options & FLAG_LOW_L)
		ft_putchar('l');
	if (options & FLAG_UP_R)
		ft_putchar('R');
	if (options & FLAG_LOW_A)
		ft_putchar('a');
	if (options & FLAG_LOW_R)
		ft_putchar('r');
	if (options & FLAG_LOW_T)
		ft_putchar('t');
	ft_putchar('\n');
}

int			add_option(char c, t_options *options)
{
	if (c == 'l')
		return (*options |= FLAG_LOW_L);
	else if (c == 'R')
		return (*options |= FLAG_UP_R);
	else if (c == 'a')
		return (*options |= FLAG_LOW_A);
	else if (c == 'r')
		return (*options |= FLAG_LOW_R);
	else if (c == 't')
		return (*options |= FLAG_LOW_T);
	else if (c == '\0')
		return ('\0');
	ft_putstr_fd("ft_ls : illegal option -- ", 2);
	write(2, &c, 1);
	write(2, "\n", 1);
	return (*options = -1);
}

t_options	parse_options(int argc, char **argv)
{
	int			i;
	int			j;
	t_options	options;

	i = 1;
	options = 0;
	while (i < argc)
	{
		j = 1;
		if (argv[i][0] == '-')
		{
			while (add_option(argv[i][j], &options) > 0)
				j++;
			if (options == -1)
				return (-1);
		}
		i++;
	}
	return (options);
}
