/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   padding_data.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/18 12:25:36 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/18 12:55:34 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

char	*new_padding(char *last_padding, size_t new_size)
{
	register size_t	index;
	char			*new_padding;

	index = 0;
	if (last_padding)
		garbage_free(last_padding);
	if (NULL ==
		(new_padding = (char*)garbage_malloc(sizeof(char) * (new_size + 1))))
		return (NULL);
	while (index < new_size)
	{
		new_padding[index] = ' ';
		index++;
	}
	new_padding[index] = '\0';
	return (new_padding);
}

char	*padd_hardlink(char *data_str, size_t size)
{
	static size_t	last_size;
	static char		*padding_str = NULL;
	register size_t	index;
	register int	data_index;

	last_size = 0;
	if (data_str == NULL && padding_str)
	{
		garbage_free(padding_str);
		return (NULL);
	}
	index = 0;
	data_index = ft_strlen(data_str);
	if (size != last_size || padding_str == NULL)
		padding_str = new_padding(padding_str, size);
	index = size;
	while (data_str + data_index >= data_str)
	{
		padding_str[index] = data_str[data_index];
		data_index--;
		index--;
	}
	return (padding_str);
}

char	*padd_user_name(char *data_str, size_t size)
{
	static size_t	last_size;
	static char		*padding_str = NULL;
	register size_t	index;

	last_size = 0;
	if (data_str == NULL && padding_str)
	{
		garbage_free(padding_str);
		return (NULL);
	}
	index = 0;
	if (size != last_size || padding_str == NULL)
		padding_str = new_padding(padding_str, size);
	index = 0;
	while (data_str[index])
	{
		padding_str[index] = data_str[index];
		index++;
	}
	return (padding_str);
}

char	*padd_group_name(char *data_str, size_t size)
{
	static size_t	last_size;
	static char		*padding_str = NULL;
	register size_t	index;

	last_size = 0;
	if (data_str == NULL && padding_str)
	{
		garbage_free(padding_str);
		return (NULL);
	}
	index = 0;
	if (size != last_size || padding_str == NULL)
		padding_str = new_padding(padding_str, size);
	index = 0;
	while (data_str[index])
	{
		padding_str[index] = data_str[index];
		index++;
	}
	return (padding_str);
}

char	*padd_size(char *data_str, size_t size)
{
	static size_t	last_size;
	static char		*padding_str = NULL;
	register size_t	index;
	register int	data_index;

	last_size = 0;
	if (data_str == NULL && padding_str)
	{
		garbage_free(padding_str);
		return (NULL);
	}
	index = 0;
	data_index = ft_strlen(data_str);
	if (size != last_size || padding_str == NULL)
		padding_str = new_padding(padding_str, size);
	index = size;
	while (data_str + data_index >= data_str)
	{
		padding_str[index] = data_str[data_index];
		data_index--;
		index--;
	}
	return (padding_str);
}
