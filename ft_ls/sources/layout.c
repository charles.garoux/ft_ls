/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   layout.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/19 15:59:31 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/19 15:59:56 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

static void		update_layout_special(t_layout *layout, t_list_elem *element)
{
	size_t len;

	if (element->type == 'c' || element->type == 'b')
	{
		len = ft_strlen(element->major);
		layout->major_max = (len > layout->major_max) ? len : layout->major_max;
		len = ft_strlen(element->minor);
		layout->minor_max = (len > layout->minor_max) ? len : layout->minor_max;
		if (layout->major_max + 2 + layout->minor_max > layout->size_max)
			len = layout->major_max + 2 + layout->minor_max;
	}
	else
		len = ft_strlen(element->size);
	layout->size_max = (len > layout->size_max) ? len : layout->size_max;
	if (layout->major_max &&
		layout->major_max + 2 + layout->minor_max < layout->size_max)
		layout->major_max = layout->size_max - (2 + layout->minor_max);
}

void			update_layout(t_layout *layout, t_list_elem *element)
{
	size_t len;

	len = ft_strlen(element->hardlink);
	layout->hardlink_max = (len > layout->hardlink_max) ?
		len : layout->hardlink_max;
	len = ft_strlen(element->user_name);
	layout->user_name_max = (len > layout->user_name_max) ?
		len : layout->user_name_max;
	len = ft_strlen(element->group_name);
	layout->group_name_max = (len > layout->group_name_max) ?
		len : layout->group_name_max;
	update_layout_special(layout, element);
}

t_layout		*create_layout(void)
{
	t_layout	*new_layout;

	if (NULL == (new_layout = (t_layout*)garbage_malloc(sizeof(*new_layout))))
	{
		DEBUG_PRINT("Erreur", "malloc() == NULL");
		return (NULL);
	}
	new_layout->hardlink_max = 0;
	new_layout->user_name_max = 0;
	new_layout->group_name_max = 0;
	new_layout->size_max = 0;
	new_layout->major_max = 0;
	new_layout->minor_max = 0;
	return (new_layout);
}
