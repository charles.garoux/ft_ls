/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   sort_list.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/18 12:17:42 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/18 12:20:45 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** Retour des fonctions de comparaison :
** > 0  (superieur)
** == 0 (egaux)
** < 0 (inferieur)
*/

int			cmp_name_ascii(t_list_elem *a, t_list_elem *b)
{
	char	*a_name;
	char	*b_name;

	a_name = (a->name[0] == '.') ? a->name + 1 : a->name;
	b_name = (b->name[0] == '.') ? b->name + 1 : b->name;
	return (ft_strcmp(a_name, b_name));
}

int			cmp_modif_date(t_list_elem *a, t_list_elem *b)
{
	return (b->modif_date_timestamp - a->modif_date_timestamp);
}

t_list_elem	*get_last_element(t_list_elem *root)
{
	while (root && root->next)
		root = root->next;
	return (root);
}

/*
** Trie a liste d'on le premier element est entre.
**  Retourne l'element en tete de liste
*/

t_list_elem	*sort_list(t_list_elem *begin_element, t_options options)
{
	t_elem_cmp			ft_cmp;
	t_list_elem			*pivot;

	if (options & FLAG_LOW_T)
		ft_cmp = &cmp_modif_date;
	else
		ft_cmp = &cmp_name_ascii;
	pivot = get_last_element(begin_element);
	if (options & FLAG_LOW_R)
		rev_quick_sort(begin_element, pivot, ft_cmp);
	else
		quick_sort(begin_element, pivot, ft_cmp);
	return (begin_element);
}
