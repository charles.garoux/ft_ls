/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   padding_major_minor.c                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/18 12:56:15 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/18 12:56:58 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

char	*padd_major(char *data_str, size_t size)
{
	static size_t	last_size;
	static char		*padding_str = NULL;
	register size_t	index;
	register int	data_index;

	last_size = 0;
	if (data_str == NULL && padding_str)
	{
		garbage_free(padding_str);
		return (NULL);
	}
	index = 0;
	data_index = ft_strlen(data_str);
	if (size != last_size || padding_str == NULL)
		padding_str = new_padding(padding_str, size);
	index = size;
	while (data_str + data_index >= data_str)
	{
		padding_str[index] = data_str[data_index];
		data_index--;
		index--;
	}
	return (padding_str);
}

char	*padd_minor(char *data_str, size_t size)
{
	static size_t	last_size;
	static char		*padding_str = NULL;
	register size_t	index;
	register int	data_index;

	last_size = 0;
	if (data_str == NULL && padding_str)
	{
		garbage_free(padding_str);
		return (NULL);
	}
	index = 0;
	data_index = ft_strlen(data_str);
	if (size != last_size || padding_str == NULL)
		padding_str = new_padding(padding_str, size);
	index = size;
	while (data_str + data_index >= data_str)
	{
		padding_str[index] = data_str[data_index];
		data_index--;
		index--;
	}
	return (padding_str);
}
