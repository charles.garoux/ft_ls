/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_extended_element.c                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/19 11:12:21 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/19 11:17:54 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_next_data(char *str)
{
	write(1, " ", 1);
	write(1, str, ft_strlen(str));
}

void	print_extended_element_no_layout(t_list_elem *element)
{
	ft_putchar(element->type);
	ft_putstr(element->right);
	print_next_data(element->hardlink);
	print_next_data(element->user_name);
	print_next_data(element->group_name);
	if (element->type == 'c' || element->type == 'b')
	{
		print_next_data(element->major);
		write(1, ",", 1);
		print_next_data(element->minor);
	}
	else
		print_next_data(element->size);
	print_next_data(element->modif_date);
	print_next_data(element->name);
	if (element->type == 'l' && element->link_target != NULL)
	{
		write(1, " ->", 3);
		print_next_data(element->link_target);
	}
	ft_putchar('\n');
}

void	print_extended_element_padding_special(t_list_elem *element,\
		t_layout *layout)
{
	char	*major_minor_str;
	char	*size_str;

	size_str = padd_size(element->size, layout->size_max);
	if (element->type == 'c' || element->type == 'b')
	{
		major_minor_str = triple_conctat(\
				padd_major(element->major, layout->major_max), ", ",\
				padd_minor(element->minor, layout->minor_max));
		print_next_data(major_minor_str);
	}
	else
		print_next_data(size_str);
	print_next_data(element->modif_date);
	print_next_data(element->name);
	if (element->type == 'l' && element->link_target != NULL)
	{
		write(1, " ->", 3);
		print_next_data(element->link_target);
	}
}

void	print_extended_element_padding(t_list_elem *element, t_layout *layout)
{
	char	*hardlink_str;
	char	*user_name_str;
	char	*group_name_str;

	hardlink_str = padd_hardlink(element->hardlink, layout->hardlink_max);
	user_name_str = padd_user_name(element->user_name, layout->user_name_max);
	group_name_str = padd_group_name(element->group_name,\
			layout->group_name_max);
	ft_putchar(element->type);
	ft_putstr(element->right);
	print_next_data(hardlink_str);
	print_next_data(user_name_str);
	print_next_data(group_name_str);
	print_extended_element_padding_special(element, layout);
	ft_putchar('\n');
}

void	print_extended_element(t_list_elem *element, t_layout *layout)
{
	if (layout == NULL)
		print_extended_element_no_layout(element);
	else
		print_extended_element_padding(element, layout);
}
