/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   element_filling.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/18 17:37:52 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/18 18:53:10 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

char	*get_hardlink_nbr(nlink_t st_nlink)
{
	char	*hardlink;

	if (NULL == (hardlink = ft_lltoa(st_nlink)))
	{
		DEBUG_PRINT("Erreur", "ft_lltoa() == NULL");
		return (NULL);
	}
	return (hardlink);
}

void	set_special_right(t_list_elem *element, mode_t right)
{
	if (right & S_ISUID)
		element->right[2] = (element->right[2] == 'x') ? 's' : 'S';
	if (right & S_ISUID)
		element->right[5] = (element->right[5] == 'x') ? 's' : 'S';
	if (right & S_ISUID)
		element->right[8] = (element->right[8] == 'x') ? 't' : 'T';
}

void	set_right(t_list_elem *element, mode_t right)
{
	element->right[0] = (right & S_IRUSR) ? 'r' : '-';
	element->right[1] = (right & S_IWUSR) ? 'w' : '-';
	element->right[2] = (right & S_IXUSR) ? 'x' : '-';
	element->right[3] = (right & S_IRGRP) ? 'r' : '-';
	element->right[4] = (right & S_IWGRP) ? 'w' : '-';
	element->right[5] = (right & S_IXGRP) ? 'x' : '-';
	element->right[6] = (right & S_IROTH) ? 'r' : '-';
	element->right[7] = (right & S_IWOTH) ? 'w' : '-';
	element->right[8] = (right & S_IXOTH) ? 'x' : '-';
	element->right[9] = 0;
	set_special_right(element, right);
}

char	*get_group_name(gid_t gid)
{
	struct group	*group_data;
	char			*group_name;

	if (NULL == (group_data = getgrgid(gid)))
	{
		if (NULL == (group_name = ft_itoa(gid)))
		{
			DEBUG_PRINT("Erreur", "ft_itoa() == NULL");
			return (NULL);
		}
	}
	else if (NULL == (group_name = ft_strdup(group_data->gr_name)))
	{
		DEBUG_PRINT("Erreur", "ft_strdup() == NULL");
		return (NULL);
	}
	return (group_name);
}
