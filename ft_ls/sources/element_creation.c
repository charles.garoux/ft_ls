/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   element_creation.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/18 13:00:14 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/08/24 13:22:12 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_ls.h"

int			unlink_element(t_list_elem *element)
{
	if (element->next)
		element->next->prev = element->prev;
	if (element->prev)
		element->prev->next = element->next;
	if (element->prev == NULL && element->parent
			&& element->parent != (t_directory*)1)
		element->parent->begin_content = element->next;
	return (1);
}

int			free_element(t_list_elem *element)
{
	unlink_element(element);
	if (element->dir_data != NULL)
		free_directory_element(element);
	garbage_free(element->name);
	garbage_free(element->path);
	garbage_free(element->user_name);
	garbage_free(element->group_name);
	garbage_free(element->modif_date);
	garbage_free(element->size);
	garbage_free(element->hardlink);
	if (element->type == 'l' && element->link_target)
		garbage_free(element->link_target);
	if (element->link_element)
		free_element(element->link_element);
	garbage_free(element->major);
	garbage_free(element->minor);
	garbage_free(element);
	return (1);
}

int			free_element_list(t_list_elem *element)
{
	t_list_elem *next;

	if (element == NULL)
		return (0);
	next = element->next;
	while (element != NULL)
	{
		next = element->next;
		free_element(element);
		element = next;
	}
	return (0);
}

/*
** Une fois les DEBUG_PRINT supprimer, create_element() est a la norme
*/

t_list_elem	*create_element(char *path, t_directory *parent_dir,
		t_list_elem *prev_element, t_layout *layout)
{
	struct stat	data;
	t_list_elem	*new_element;

	if (lstat(path, &data) == -1)
	{
		DEBUG_PRINT("Erreur", "lstat() == NULL");
		return ((t_list_elem*)(print_error(path) + 1));
	}
	if (NULL ==
			(new_element = (t_list_elem*)garbage_malloc(sizeof(t_list_elem))))
	{
		DEBUG_PRINT("Erreur", "malloc() == NULL");
		return (NULL);
	}
	new_element->parent = parent_dir;
	if (new_element->parent)
		new_element->parent->block += data.st_blocks;
	if (new_element->parent && new_element->parent->begin_content == NULL)
		new_element->parent->begin_content = new_element;
	if (fill_element(new_element, &data, path, parent_dir) == -1)
	{
		DEBUG_PRINT("Erreur", "fill_element() == -1");
		return (NULL);
	}
	if (prev_element != NULL)
	{
		new_element->prev = prev_element;
		prev_element->next = new_element;
	}
	if (layout)
		update_layout(layout, new_element);
	return (new_element);
}

t_list_elem	*create_target_link_element(char *link_path, char *link_path_target)
{
	struct stat	data;
	t_list_elem	*new_element;

	if (stat(link_path, &data) == -1)
		return ((t_list_elem*)(print_error(link_path_target) + 1));
	if (NULL ==
			(new_element = (t_list_elem*)garbage_malloc(sizeof(t_list_elem))))
	{
		DEBUG_PRINT("Erreur", "malloc() == NULL");
		return (NULL);
	}
	new_element->parent = NULL;
	if (fill_element(new_element, &data, link_path_target, NULL) == -1)
	{
		DEBUG_PRINT("Erreur", "fill_element() == -1");
		return (NULL);
	}
	return (new_element);
}
