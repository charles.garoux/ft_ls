#include <sys/types.h>
#include <sys/dir.h>
#include <sys/stat.h>
#include <stdio.h>

int main(void)
{
	printf("Selon si __DARWIN_64_BIT_INO_T existe ou non la structure \"dirent\" sera differente.\n\n");
	
	#if __darwin_64_bit_ino_t
	printf("__darwin_64_bit_ino_t=%d\n", __darwin_64_bit_ino_t);
	#endif
	#if !__darwin_64_bit_ino_t
	printf("__darwin_64_bit_ino_t n'existe pas\n");
	#endif

	#if _DARWIN_FEATURE_64_BIT_INODE
	printf("_DARWIN_FEATURE_64_BIT_INODE=%d\n", _DARWIN_FEATURE_64_BIT_INODE);
	#endif
	#if !_DARWIN_FEATURE_64_BIT_INODE
	printf("_DARWIN_FEATURE_64_BIT_INODE n'existe pas\n");
	#endif

	printf("__DARWIN_MAXPATHLEN=%d\n", __DARWIN_MAXPATHLEN);
	printf("__DARWIN_MAXNAMLEN=%d\n", __DARWIN_MAXNAMLEN);
	printf("MAXNAMLEN=%d\n", MAXNAMLEN);
	return (0);
}
