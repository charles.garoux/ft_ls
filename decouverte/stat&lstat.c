#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>

void print_data(struct stat buf)
{
	printf("\tst_ino=%llu\n", buf.st_ino);
	printf("\tst_size=%lld\n", buf.st_size);
	printf("\tst_mode=%d\n", buf.st_mode & S_IFMT);
	printf("\tst_blocks=%lld\n", buf.st_blocks);	
	printf("S_ISREG(%d)=%d\n", buf.st_mode, S_ISREG(buf.st_mode));
}

int main (int argc, char **argv)
{
	struct stat buf;

	//	buf = (struct stat*)malloc(sizeof(*buf));

	if (argc < 2)
	{
		printf ("Usage: %s <dirname>\n", argv[0]);
		return 1;
	}

	if (stat(argv[1], &buf) == -1)
		perror("stat()");
	else
	{
		printf("stat()\n");
		print_data(buf);
	}

	if (lstat(argv[1], &buf) == -1)
		perror("stat()");
	else
	{
		printf("lstat()\n");
		print_data(buf);
	}
//	if ((buf.st_mode & S_IFMT) == S_IFREG)
//		printf("fichier regulier\n");
	printf("\n");
	switch (buf.st_mode & S_IFMT) {
		case S_IFBLK:  printf("block device\n");            break;
		case S_IFCHR:  printf("character device\n");        break;
		case S_IFDIR:  printf("directory\n");               break;
		case S_IFIFO:  printf("FIFO/pipe\n");               break;
		case S_IFLNK:  printf("symlink\n");                 break;
		case S_IFREG:  printf("regular file\n");            break;
		case S_IFSOCK: printf("socket\n");                  break;
		default:       printf("unknown?\n");                break;
	}

	return 0;
}
