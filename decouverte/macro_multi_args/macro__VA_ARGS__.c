#define MY_MACRO(fct, ...)		fct(__VA_ARGS__);
#include <stdio.h>

int		add(int a, int b)
{
	printf("%d\n", a + b);
	return (a + b);
}

int		main(int argc, char **argv)
{
	MY_MACRO(add, 5, 8);
	return (0);
}
