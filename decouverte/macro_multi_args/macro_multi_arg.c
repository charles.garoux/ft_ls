#define MY_MACRO(fct, args...)		fct(args);
#include <stdio.h>

int		add(int a, int b)
{
	printf("%d\n", a + b);
	return (a + b);
}

int		main(int argc, char **argv)
{
	MY_MACRO(add, 5, 8);
	return (0);
}
