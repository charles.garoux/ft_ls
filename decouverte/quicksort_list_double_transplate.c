// A C program to sort a linked list using Quicksort 
#include <stdio.h> 

/* a node of the doubly linked list */
struct Node 
{ 
	int data; 
	struct Node *next; 
	struct Node *prev; 
}; 

/* A utility function to swap two elements */
void swap ( int* a, int* b ) 
{ int t = *a;	 *a = *b;	 *b = t; } 

// A utility function to find last node of linked list 
struct Node *lastNode(struct Node *root) 
{ 
	while (root && root->next) 
		root = root->next; 
	return root; 
} 

/* Considers last element as pivot, places the pivot element at its 
correct position in sorted array, and places all smaller (smaller than 
pivot) to left of pivot and all greater elements to right of pivot */
struct Node* partition(struct Node *left, struct Node *pivot) 
{ 
	// set pivot as pivot element 
	int pivot_value = pivot->data; 

	// similar to prev_partition = left-1 for array implementation 
	struct Node *prev_start = left->prev; 

	// Similar to "for (int index = left; index <= pivot- 1; index++)" 
	for (struct Node *index = left; index != pivot; index = index->next) 
	{ 
		if (index->data <= pivot_value) 
		{ 
			// Similar to prev_start++ for array 
			prev_start = (prev_start == NULL)? left : prev_start->next; 

			swap(&(prev_start->data), &(index->data)); 
		} 
	} 
	prev_start = (prev_partition == NULL)? left : prev_start->next; // Similar to prev_start++ 
	swap(&(prev_start->data), &(pivot->data)); 
	return prev_start; 
} 

/* A recursive implementation of quicksort for linked list */
void _quickSort(struct Node* start, struct Node *end) 
{ 
	if (end != NULL && start != end && start != end->next) 
	{ 
		struct Node *p = partition(start, end); 
		_quickSort(start, p->prev); 
		_quickSort(p->next, end); 
	} 
} 

// The main function to sort a linked list. It mainly calls _quickSort() 
void quickSort(struct Node *head) 
{ 
	// Find last node 
	struct Node *pivot = lastNode(head); 

	// Call the recursive QuickSort 
	_quickSort(head, pivot); 
} 

// A utility function to print contents of arr 
void printList(struct Node *head) 
{ 
	while (head) 
	{ 
		printf("%d ",head->data); 
		head = head->next; 
	} 
	printf("\n"); 
} 
:wq
/* Function to insert a node at the beginning of the Doubly Linked List */
void push(struct Node** head_ref, int new_data) 
{ 
	struct Node* new_node = malloc(sizeof(*new_node));	 /* allocate node */
	new_node->data = new_data; 

	/* since we are adding at the beginning, prev is always NULL */
	new_node->prev = NULL; 

	/* link the old list off the new node */
	new_node->next = (*head_ref); 

	/* change prev of head node to new node */
	if ((*head_ref) != NULL) (*head_ref)->prev = new_node ; 

	/* move the head to point to the new node */
	(*head_ref) = new_node; 
} 

/* Driver program to test above function */
int main() 
{ 
	struct Node *a = NULL; 
	push(&a, 5); 
	push(&a, 20); 
	push(&a, 4); 
	push(&a, 3); 
	push(&a, 30); 

	printf("Linked List before sorting \n"); 
	
	printList(a); 

	quickSort(a); 

	printf("Linked List after sorting \n"); 
	printList(a); 

	return 0; 
} 

