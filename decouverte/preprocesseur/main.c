#define MY_MACRO(my_fct, my_test) int		my_fct##_##my_test(void)
#define MY_STRING(my_fct_as_string)		#my_fct_as_string

#include <stdio.h>
#include "macro_tests.h"

typedef struct s_position
{
	int	x;
	int	y;
}				t_position;
#define FILL(struct_name, var1, value1, var2, value2)\
	(struct_name){.var1 = value1, .var2 = value2}

MY_MACRO(fonction_de, test_atoi)
{
	printf("the test\n");
	return (0);
}

char *ft_ret_str(char *str)
{
	return (str);
}

REGULAR_TEST_MAKER(atoi, test_101,"101", 101)
STRING_TEST_MAKER(ft_ret_str, test_101, 101, 101)

int		main(int argc, char **argv)
{
	fonction_de_test_atoi();
	printf("%s\n", MY_STRING(une_fonction_sous_forme_de_chaine));

	t_position position;
	position = FILL(t_position, x, 101, y, 42);
	printf("x=%d y=%d\n", position.x, position.y);


}

//compiler avec "gcc -E" pour voir le resultat du preprocesseur
