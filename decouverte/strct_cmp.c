/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   strct_cmp.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/06/05 13:19:26 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/05 14:13:54 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdio.h>

typedef struct s_test
{
	int uc1;
	char uc2;
}				t_test;

void	print_bits(unsigned char octet)
{
	int index;

	index = 128;
	while(index >= 1)
	{
		if(octet - index >= 0)
		{
			octet = octet - index;
			putchar('1');
		}
		else
			putchar('0');
		index = index / 2;
	}
}

int	struct_cmp(void *struct1, void *struct2, int struct_size)
{
	unsigned char	*ptr1;
	unsigned char	*ptr2;
	int				i;

	ptr1 = (unsigned char*)struct1;
	ptr2 = (unsigned char*)struct2;
	

	
	/*print_bits(ptr1[0]);
	printf("\n");
	print_bits(ptr1[1]);
	printf("\n");
	print_bits(ptr1[2]);
	printf("\nstruct2\n");
	print_bits(ptr2[0]);
	printf("\n");
	print_bits(ptr2[1]);
	printf("\n");
	print_bits(ptr2[2]);*/
	
	i = 0;
	while (i < struct_size)
	{
		if (ptr1[i] != ptr2[i])
			return (0);
		i++;
	}
	return (1);
}

/*
 * Fonction final
*/

int	ft_structcmp(void *struct1, void *struct2, size_t struct_size)
{
	unsigned char	*ptr1;
	unsigned char	*ptr2;
	size_t			i;

	ptr1 = (unsigned char*)struct1;
	ptr2 = (unsigned char*)struct2;
	i = 0;
	while (i < struct_size)
	{
		if (ptr1[i] != ptr2[i])
			return (0);
		i++;
	}
	return (1);
}

int	main(void)
{
	t_test	struct_test1;
	t_test	struct_test2;

	struct_test1 = (t_test){.uc1 = 1, .uc2 = 101};
	struct_test2 = (t_test){.uc1 = 1, .uc2 = 101};
	printf("sizeof(t_test)=%lu\n", sizeof(t_test));
	printf("result=%d\n", struct_cmp((void*)&struct_test1, (void*)&struct_test2, sizeof(t_test)));
	printf("result=%d\n", ft_structcmp((void*)&struct_test1, (void*)&struct_test2, sizeof(t_test)));

	return (0);
}
