#include <sys/types.h>
#include <sys/dir.h>
#include <sys/stat.h>
#include <stdio.h>

int main(void)
{
	printf("Valeur des MACRO en octal:\n");

	printf("S_IRGRP=%o\n", S_IRGRP);
	printf("S_IROTH=%o\n", S_IROTH);
	printf("S_IRWXU=%o\n", S_IRWXU);
	return (0);
}
